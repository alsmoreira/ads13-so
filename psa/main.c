/*
 =================================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 25 de abr de 2018
 Copyright   :
 Description : O algoritmo de agendamento prioritário (PSA) ou Priority Scheduling
 Algorithm  executa os processos dependendo de sua prioridade. Cada processo é
 alocado com prioridade e o processo com maior valor de prioridade é executado
 primeiro. As prioridades podem ser definidas internamente e externamente. As
 internas são decididas pelo SO dependendo do número de recursos necessários,
 tempo necessário, etc. enquanto as externas são baseadas no tempo em que o trabalho
 é necessário ou o valor a ser pago pelo trabalho realizado ou pela importância do
 processo. O agendamento prioritário pode ser preemptivo ou não-preemptivo.
 No exemplo, número de prioridade menor tem a mais alta prioridade.
 =================================================================================
 */

#include<stdio.h>

int main(int argc, char *argv[]){
    int bt[10],p[10],wt[10],tat[10],pr[10],i,j,n,total=0,pos,temp;
    float avg_wt,avg_tat;

    printf("Insira o numero total de processos (max 10) : ");
    scanf("%d",&n);

    printf("\nInsira o tempo de execucao e a prioridade\n");
    for(i=0;i<n;i++) {
        printf("\nP[%d]\n",i+1);
        printf("Tempo-execucao : ");
        scanf("%d",&bt[i]);
        printf("Prioridade : ");
        scanf("%d",&pr[i]);
        p[i]=i+1;           //contém o número do processo
    }

    //ordena tempo de execução, prioridade e número de processo em ordem crescente usando selection sort
    for(i=0;i<n;i++){
        pos=i;
        for(j=i+1;j<n;j++){
            if(pr[j]<pr[pos])
                pos=j;
        }

        temp=pr[i];
        pr[i]=pr[pos];
        pr[pos]=temp;

        temp=bt[i];
        bt[i]=bt[pos];
        bt[pos]=temp;

        temp=p[i];
        p[i]=p[pos];
        p[pos]=temp;
    }

    wt[0]=0;    //tempo de espera para o primeiro processo é zero 0

    //calcula o tempo de espera
    for(i=1;i<n;i++){
        wt[i]=0;
        for(j=0;j<i;j++)
            wt[i]+=bt[j];
        total+=wt[i];
    }

    avg_wt=total/n;      //média do tempo de espera
    total=0;

    printf("\nProcesso\tTempo-execucao\tTempo-espera\tTempo-turnaround");
    for(i=0;i<n;i++){
        tat[i]=bt[i]+wt[i];     //calculo do tempo de turnaround
        total+=tat[i];
        printf("\nP[%d]\t\t%d\t\t%d\t\t%d",p[i],bt[i],wt[i],tat[i]);
    }

    avg_tat=total/n;     //média do tempo de turnaround
    printf("\n\nMedia do tempo de espera = %.5e",avg_wt);
    printf("\nMedia do tempo de turnaround = %.5e\n",avg_tat);

    return 0;
}
