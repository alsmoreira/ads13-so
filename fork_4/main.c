/*
 * main.c
 *
 *  Created on: 22 de nov. de 2020
 *      Author: anderson
 *      Description: Uso do fork(), wait() e exec() porém com a saída padrão
 *      para um arquivo.
 *      Use: Para compilar digitar em um prompt make, e para executar ./fork_4.exe
 *      Padrão C99 utilizando Cygwin
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    int rc = fork();
    if (rc < 0) {
        fprintf(stderr, "fork falhou\n");
        exit(1);
    } else if (rc == 0) {
	// filho: redireciona saída padrão para um arquivo
	close(STDOUT_FILENO);
	open("fork.out", O_CREAT|O_WRONLY|O_TRUNC, S_IRWXU);

	// agora executa "wc"...
        char *myargs[3];
        myargs[0] = strdup("wc");
        myargs[1] = strdup("main.c");
        myargs[2] = NULL;
        execvp(myargs[0], myargs);
    } else {
        int wc = wait(NULL);
        assert(wc >= 0);
    }
    return 0;
}


