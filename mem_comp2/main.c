/*
 * main.c
 *
 *  Created on: 15 de apr. de 2019
 *  Author: anderson
 *  Description: O código demonstra como dois processos podem interagir usando a
 *  memória compartilhada. O código é o mesmo do exemplo mem_comp1, exceto que
 *  depois de imprimir a string "Ola!" o processo principal chama um fork, e
 *  um processo filho é gerado, que armazena uma string diferente no mesmo endereço.
 *  Enquanto isso, o processo pai suspende, espera que o filho termine e sai com
 *  código de sucesso; a string recém-armazenada é impressa no console. Se vários
 *  processos precisarem modificar e acessar segmentos de memória compartilhada
 *  simultaneamente, o programador precisa empregar algumas ferramentas de sincronização,
 *  como semáforos (veremos mais adiante como funciona).
 *  Teste foi realizado utilizando WSL versão 1.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/shm.h>

enum {SEGMENT_SIZE = 0x6400};

const char *data = "Ola";

int main(int argc, char *argv[]) {

    int status;
    int segment_id;

    segment_id = shmget (IPC_PRIVATE, SEGMENT_SIZE, IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
    char *sh_mem = (char *) shmat(segment_id, 0, 0);

    printf("ID do segmento %d\n", segment_id);
    printf("Anexado a %p\n", sh_mem);
    memmove(sh_mem, data, strlen(data)+1);
    printf("%s\n", sh_mem);

    pid_t child_pid;
	child_pid = fork();

    if (child_pid == -1)
        perror("Erro no fork!");

    if (child_pid == 0) {
        strcpy(sh_mem, "NOVOS DADOS Armazenados pelo processo filho\0");
        printf("pid do filho - %d\n", getpid());
        exit(EXIT_SUCCESS);
    } else {
        pid_t ret = waitpid(child_pid, &status, WUNTRACED | WCONTINUED);
        if (ret == -1)
            perror("waitpid");

        if (WIFEXITED(status))
            printf("Filho finalizado, status - %d\n", WEXITSTATUS(status));

        if (WEXITSTATUS(status) == 0)
            printf("%s\n", sh_mem);
    }

    shmdt(sh_mem);
    shmctl(segment_id, IPC_RMID, 0);
    exit(EXIT_SUCCESS);
}
