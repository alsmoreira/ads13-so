/*
 ============================================================================
 Name        : main.c (padrão 99)
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 15 de aug. de 2019
 Copyright   :
 Description : Este programa que aloca uma memória chamando malloc().
 Primeiro, ele aloca um pouco de memória (linha 32). Em seguida, ele imprime
 o endereço da memória (34) e, em seguida, coloca o número zero no primeiro
 slot da memória recém-alocada (35). Por fim, ele faz loops, atrasando por
 um segundo e incrementando o valor armazenado no endereço mantido em p. A
 cada impressão do estado, ele também imprime o que é chamado de identificador
 de processo (o PID) do programa em execução. Este PID é único por processo em
 execução.
 Uso: Para compilar utilizar algum sistema POSIX, neste caso utilizou
 o CYGWIN no Windows, digitar em um prompt $make e para executar ./mem.exe 1
 ============================================================================
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include "../include/comun.h"

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "uso: mem <valor>\n");
        exit(1);
    }
    int *p;
    p = malloc(sizeof(int));//a mágica ocorre aqui
    assert(p != NULL);
    printf("(%d) endereco apontado por p: %p\n", (int) getpid(), p);
    *p = atoi(argv[1]); // determina uma valor para o endereço armazenado em p
    while (1) {
        Spin(1);
        *p = *p + 1;
        printf("(%d) valor de p: %d\n", getpid(), *p);
    }
    return 0;
}
