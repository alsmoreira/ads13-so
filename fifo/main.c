/*
 ============================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 25 de abr de 2018
 Copyright   :
 Description : FIFO (First In First Out) ou Primeiro que chega é o primeiro a
 sair é um algoritmo de escalonamento do tipo não-preemptivo. A estratégia do
 SO é atribuir prioridade elevado ao processo na ordem em que este solicita ao
 processador. O processo que solicita a UCP primeiro é alocado inicialmente.
 É facilmente implementado com uma fila para gerenciar as tarefas. Á medida que
 os processos entram, estes são colocados no fim da fila. Á medida que a UCP
 termina cada tarefa, ela a remove desde o início da fila e segue para a próxima
 tarefa.
 Use: Compilado com POSIX C99 - CYGWIN. Em um prompt digite: make e para
 executar ./fifo.exe
 ============================================================================
 */

#include<stdio.h>

int main(){
    int n,bt[10],wt[10],tat[10],avwt=0,avtat=0,i,j;
    printf("Entre com o numero total de processos (maximo 10):");
    scanf("%d",&n);

    printf("\nEntre com o tempo de execucao do processo\n");
    for(i=0;i<n;i++){
        printf("P[%d]:",i+1);
        scanf("%d",&bt[i]);
    }

    wt[0]=0;    //tempo de espero do primeiro processo é 0

    //calcular o tempo de espera
    for(i=1;i<n;i++){
        wt[i]=0;
        for(j=0;j<i;j++)
            wt[i]+=bt[j];
    }

    printf("\nProcesso\tTempo-execucao\tTempo-espera\tTempo-turnaround");

    //calcular tempo de turnaround
    for(i=0;i<n;i++){
        tat[i]=bt[i]+wt[i];
        avwt+=wt[i];
        avtat+=tat[i];
        printf("\nP[%d]\t\t%d\t\t%d\t\t%d",i+1,bt[i],wt[i],tat[i]);
    }

    //TO-DO: aprimorar calculo do tempo
    avwt/=i;
    avtat/=i;
    printf("\n\nMedia do tempo de espera:%d",avwt);
    printf("\nMedia do tempo de turnaround:%d",avtat);

    return 0;
}
