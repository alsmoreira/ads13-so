/*
 * main.c
 *
 *  Created on: 14 de dez. de 2021
 *      Author: anderson
 *      Description: Utilização de um pipe demonstrando como funciona
 *      a comunicação entre processo e a utilização de um fork para isso.
 *      Observe a utilização do descritor de arquivos FD se for 0
 *      é para leitura, se for 1 é para escrita.
 *      Testado no CYGWIN, padrão POSIX C99
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>

int main(int argc,char* argv[]){
    int fd[2];
    //fd[0] - ler - output
    //fd[1] - escreve - input
    if (pipe(fd) == -1){
        printf("Um erro ocorreu na abertura do pipe\n");
        return 1;
    }
    int id = fork();
    if (id == -1){
        printf("Ocorreu um erro no fork\n");
        return 4;
    }
    if (id == 0){
        close(fd[0]);
        int x;
        printf("Entre com um numero: "); //executou no filho
        scanf("%d", &x);
        if (write(fd[1], &x, sizeof(int)) == -1){
            printf("Um erro ocorreu ao escrever no pipe\n");
            return 2;
        }
        close(fd[1]);
    } else {
        close(fd[1]);
        int y;
        if (read(fd[0], &y, sizeof(int)) == -1){
            printf("Um erro ocorreu lendo o pipe\n");
        }
        y = y * 10;
        close(fd[0]);
        printf("Buscou do processo filho %d\n", y);
    }
    return 0;
}
