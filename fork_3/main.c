/*
 * main.c
 *
 *  Created on: 22 de nov. de 2020
 *      Author: anderson moreira
 *      Description: Utilização do fork() e a chamada de sistema exec()
 *      Use: Para compilar digita make em um prompt de terminal e para executar ./fork_3.exe
 *      Ulizando CYGWIN (padão C99 POSIX)
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    printf("ola mundo (pid:%d)\n", (int) getpid());
    int rc = fork();
    if (rc < 0) {
        fprintf(stderr, "fork falhou\n");
        exit(1);
    } else if (rc == 0) {
        printf("oi, eu sou um filho (pid:%d)\n", (int) getpid());
        char *myargs[3];
        myargs[0] = strdup("wc");   // programa: "wc" (contador de palavras)
        myargs[1] = strdup("main.c"); // argumento: arquivo para contar
        myargs[2] = NULL;           // marca fim do array
        execvp(myargs[0], myargs);  // executa o contador de palavras
        printf("isso não deve imprimir");
    } else {
        int wc = wait(NULL);
        printf("oi, eu sou pai de %d (wc:%d) (pid:%d)\n", rc, wc, (int) getpid());
    }
    return 0;
}

