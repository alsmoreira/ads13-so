#! /usr/bin/env python
#anderson 

import sys
from optparse import OptionParser
import random

parser = OptionParser()
parser.add_option("-s", "--seed", default=0, help="a semente aleatoria", 
                  action="store", type="int", dest="seed")
parser.add_option("-j", "--jobs", default=3, help="numero de tarefas no sistema",
                  action="store", type="int", dest="jobs")
parser.add_option("-l", "--jlist", default="", help="em vez de tarefas aleatorias, forneca uma lista separada por virgulas de tempos de execucao",
                  action="store", type="string", dest="jlist")
parser.add_option("-m", "--maxlen", default=10, help="duracao maxima da tarefa",
                  action="store", type="int", dest="maxlen")
parser.add_option("-p", "--policy", default="FIFO", help="politica de escalonamento em uso: FIFO, SJF, RR",
                  action="store", type="string", dest="policy")
parser.add_option("-q", "--quantum", help="fatia de tempo para a politica RR", default=1, 
                  action="store", type="int", dest="quantum")
parser.add_option("-c", help="computar respostas para mim", action="store_true", default=False, dest="solve")

(options, args) = parser.parse_args()

random.seed(options.seed)

print ('ARG politica', options.policy)
if options.jlist == '':
    print ('ARG tarefas', options.jobs)
    print ('ARG max_duracao', options.maxlen)
    print ('ARG semente', options.seed)
else:
    print ('ARG jlist', options.jlist)

print ('')

print ('Aqui esta a lista de tarefas, com o tempo de execucao de cada uma: ')

import operator

joblist = []
if options.jlist == '':
    for jobnum in range(0,options.jobs):
        runtime = int(options.maxlen * random.random()) + 1
        joblist.append([jobnum, runtime])
        print ('  Tarefa', jobnum, '( duracao = ' + str(runtime) + ' )')
else:
    jobnum = 0
    for runtime in options.jlist.split(','):
        joblist.append([jobnum, float(runtime)])
        jobnum += 1
    for job in joblist:
        print ('  Tarefa', job[0], '( duracao = ' + str(job[1]) + ' )')
print ('\n')

if options.solve == True:
    print ('** Solucoes **\n')
    if options.policy == 'SJF':
        joblist = sorted(joblist, key=operator.itemgetter(1))
        options.policy = 'FIFO'
    
    if options.policy == 'FIFO':
        thetime = 0
        print ('Rastreamento da execucao:')
        for job in joblist:
            print ('  [ tempo %3d ] Executa tarefa %d por %.2f segs ( FEITO em %.2f )' % (thetime, job[0], job[1], thetime + job[1]))
            thetime += job[1]

        print ('\nEstatistica final:')
        t     = 0.0
        count = 0
        turnaroundSum = 0.0
        waitSum       = 0.0
        responseSum   = 0.0
        for tmp in joblist:
            jobnum  = tmp[0]
            runtime = tmp[1]
            
            response   = t
            turnaround = t + runtime
            wait       = t
            print ('  Tarefa %3d -- Resposta: %3.2f  Turnaround %3.2f  Espera %3.2f' % (jobnum, response, turnaround, wait))
            responseSum   += response
            turnaroundSum += turnaround
            waitSum       += wait
            t += runtime
            count = count + 1
        print ('\n  Media -- Resposta: %3.2f  Turnaround %3.2f  Espera %3.2f\n' % (responseSum/count, turnaroundSum/count, waitSum/count))
                     
    if options.policy == 'RR':
        print ('Rastreamento da execucao:')
        turnaround = {}
        response = {}
        lastran = {}
        wait = {}
        quantum  = float(options.quantum)
        jobcount = len(joblist)
        for i in range(0,jobcount):
            lastran[i] = 0.0
            wait[i] = 0.0
            turnaround[i] = 0.0
            response[i] = -1

        runlist = []
        for e in joblist:
            runlist.append(e)

        thetime  = 0.0
        while jobcount > 0:
            # print ('%d tarefas faltantes' % jobcount)
            job = runlist.pop(0)
            jobnum  = job[0]
            runtime = float(job[1])
            if response[jobnum] == -1:
                response[jobnum] = thetime
            currwait = thetime - lastran[jobnum]
            wait[jobnum] += currwait
            if runtime > quantum:
                runtime -= quantum
                ranfor = quantum
                print ('  [ tempo %3d ] Executa tarefa %3d por %.2f segs' % (thetime, jobnum, ranfor))
                runlist.append([jobnum, runtime])
            else:
                ranfor = runtime;
                print ('  [ tempo %3d ] Executa tarefa %3d por %.2f segs ( FEITO em %.2f )' % (thetime, jobnum, ranfor, thetime + ranfor))
                turnaround[jobnum] = thetime + ranfor
                jobcount -= 1
            thetime += ranfor
            lastran[jobnum] = thetime

        print ('\nEstatistica final:')
        turnaroundSum = 0.0
        waitSum       = 0.0
        responseSum   = 0.0
        for i in range(0,len(joblist)):
            turnaroundSum += turnaround[i]
            responseSum += response[i]
            waitSum += wait[i]
            print ('  Tarefa %3d, -- Resposta: %3.2f,  Turnaround %3.2f, Espera %3.2f' % (i, response[i], turnaround[i], wait[i]))
        count = len(joblist)
        
        print ('\n  Media -- Resposta: %3.2f  Turnaround %3.2f  Espera %3.2f\n' % (responseSum/count, turnaroundSum/count, waitSum/count))

    if options.policy != 'FIFO' and options.policy != 'SJF' and options.policy != 'RR': 
        print ('Error: Politica ', options.policy, ' nao esta disponivel.')
        sys.exit(0)
else:
    print ('Calcule o turnaround, o tempo de resposta e o tempo de espera para cada tarefa.')
    print ('Quando finalizar, execute o programa novamente, com os mesmos parametros,')
    print ('porem com -c, no qual ira mostrar as respostas. Pode usar')
    print ('-s <algumnumero> ou a sua propria lista de tarefas (-l 10,15,20 por exemplo)')
    print ('para gerar problemas diferentes para uso.')
    print ('')



