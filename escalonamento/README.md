# Visão geral

Este programa, escala.py, permite que você veja o desempenho de diferentes políticas de escalonamento em métricas de agendamento, como tempo de resposta, tempo de retorno e tempo total de espera. 

Três escalonadores são "implementados": FIFO, SJF e RR. 

Existem duas etapas para executar o programa.

Primeiro, execute sem a opção -c: isso mostra qual problema resolver sem revelar as respostas. Por exemplo, se você quiser calcular a resposta, o retorno e aguardar três tarefas usando a política FIFO, execute o seguinte:

```sh
prompt> ./escala.py -p FIFO -j 3 -s 100
```

Se não funcionar execute:
```sh
prompt> python ./escala.py -p FIFO -j 3 -s 100
```

Isso especifica a política FIFO com três tarefas e, mais importante, uma semente aleatória específica de 100. Se você quiser ver a solução para esse problema exato, você deve especificar exatamente a mesma semente aleatória novamente. Vamos executá-lo e ver o que acontece. Isto é o que você deve ver:

```sh
prompt> ./escala.py -p FIFO -j 3 -s 100
ARG politica FIFO
ARG tarefas 3
ARG max_duracao 10
ARG semente 100

Aqui esta a lista de tarefas, com o tempo de execucao de cada uma:
  Tarefa 0 ( duracao = 2 )
  Tarefa 1 ( duracao = 5 )
  Tarefa 2 ( duracao = 8 )
```

Calcule o tempo de resposta, o tempo de retorno e o tempo de espera para cada tarefa. Quando terminar, execute este programa novamente, com os mesmos argumentos, mas com a opção -c, que fornecerá as respostas. 

Você pode usar -s ou sua própria lista de tarefas (-l 10,15,20, por exemplo) para gerar problemas diferentes. Como você pode ver neste exemplo, três tarefas são geradas: tarefa 0 de tempo de execução 2, tarefa 1 de tempo de execução 5 e tarefa 2 de tempo de execução 8. 

Como verifica-se no programa, agora você pode usar isso para calcular algumas estatísticas e ver se você tem um controle sobre os conceitos básicos. Quando terminar, você pode usar o mesmo programa para "resolver" o problema e ver se fez seu trabalho corretamente. Para fazer isso, use o sinalizador "-c". A saída:

```sh
$ ./escala.py -p FIFO -j 3 -s 100 -c
ARG politica FIFO
ARG tarefas 3
ARG max_duracao 10
ARG semente 100

Aqui esta a lista de tarefas, com o tempo de execucao de cada uma:
  Tarefa 0 ( duracao = 2 )
  Tarefa 1 ( duracao = 5 )
  Tarefa 2 ( duracao = 8 )


** Solucoes **

Rastreamento da execucao:
  [ tempo   0 ] Executa tarefa 0 por 2.00 segs ( FEITO em 2.00 )
  [ tempo   2 ] Executa tarefa 1 por 5.00 segs ( FEITO em 7.00 )
  [ tempo   7 ] Executa tarefa 2 por 8.00 segs ( FEITO em 15.00 )

Estatistica final:
  Tarefa   0 -- Resposta: 0.00  Turnaround 2.00  Espera 0.00
  Tarefa   1 -- Resposta: 2.00  Turnaround 7.00  Espera 2.00
  Tarefa   2 -- Resposta: 7.00  Turnaround 15.00  Espera 7.00

  Media -- Resposta: 3.00  Turnaround 8.00  Espera 3.00
```

Como você pode ver anteriormente, o sinal -c mostra o que aconteceu. A tarefa 0 foi executada primeiro por 2 segundos, a tarefa 1 foi executada em segundo por 5 e, em seguida, a tarefa 2 foi executada por 8 segundos. Não muito difícil; afinal, é FIFO! 

O rastreamento de execução mostra esses resultados. As estatísticas finais também são úteis: elas calculam o "tempo de resposta" (o tempo que um trabalho gasta esperando após a chegada antes da primeira execução), o "tempo de retorno" (o tempo necessário para concluir o trabalho desde a primeira chegada) e o "tempo de espera" total (qualquer tempo gasto na fila de pronto, mas não em execução). 

As estatísticas são mostradas por tarefa e, em seguida, como uma média para todas. Você deve ter calculado todas essas coisas antes de executar com o sinalizador "-c"! Se você quiser tentar o mesmo tipo de problema, mas com entradas diferentes, tente alterar o número de tarefas ou a semente aleatória ou ambos. 

Diferentes sementes aleatórias basicamente fornecem uma maneira de gerar um número infinito de problemas diferentes para você, e o sinalizador "-c" permite que você verifique. Continue fazendo isso até sentir que realmente entende os conceitos. Um outro sinalizador útil é "-l" (que é um L minúsculo), que permite especificar as tarefas exatas que você deseja ver agendadas. Por exemplo, se você quiser descobrir como o SJF se sairia com três tarefas de tempo de execução 5, 10 e 15, você pode executar:

```sh
$ ./escala.py -p SJF -l 5,10,15
ARG politica SJF
ARG jlist 5,10,15

Aqui esta a lista de tarefas, com o tempo de execucao de cada uma:
  Tarefa 0 ( duracao = 5.0 )
  Tarefa 1 ( duracao = 10.0 )
  Tarefa 2 ( duracao = 15.0 )


Calcule o turnaround, o tempo de resposta e o tempo de espera para cada tarefa.
Quando finalizar, execute o programa novamente, com os mesmos parametros,
porem com -c, no qual ira mostrar as respostas. Pode usar
-s <algumnumero> ou a sua propria lista de tarefas (-l 10,15,20 por exemplo)
para gerar problemas diferentes para uso.
```

E então você pode usar -c para resolvê-lo novamente. Observe que, quando você especifica as tarefas exatas, não há necessidade de especificar uma semente aleatória ou o número de tarefas: os tempo de execução das tarefas são retirados da lista separada por vírgulas. Claro, coisas mais interessantes acontecem quando você usa políticas de escalonamento SJF (trabalho mais curto primeiro) ou mesmo RR (round robin). Experimente e veja! Se quiser ajuda:

```sh
$./escala.py -h
```

para obter uma lista completa de opções (incluindo como definir o quantum para o escalonador RR).