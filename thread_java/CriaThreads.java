import java.util.*;

public class CriaThreads {
	public static void main(String [] args) {
		int i, n = 3;

		for(i = 1; i <= n; i++) {
			Loop loop = new Loop(i);
			Thread t = new Thread(loop);
			t.start();
			System.out.println(“Thread “ + i + “ criado”);
		}
	}
}

class Loop implements Runnable {
	int j;

	public Loop (int i) { j = i; }
	public void run() {
		Random random = new Random();
		while (true) {
			System.out.println(“Thread “ + j + “ executado”);
			try {
				Thread.sleep(random.nextInt(5000));
			} catch (InterruptedException iex) { }
		}
	}
}
