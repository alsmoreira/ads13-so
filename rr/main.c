/*
 ================================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 25 de abr de 2018
 Copyright   :
 Description : O algoritmo round-robin (RR) é particularmente projetado para
 sistemas de compartilhamento de tempo. Os processos são colocados na fila pronto,
 que é uma fila circular neste caso. Assim, uma pequena unidade de tempo conhecida
 como quantum é definida. O algoritmo seleciona o primeiro processo da fila e
 executa-o pelo tempo definido pelo tempo do quantum. Se o processo tem tempo de
 execução menor do que o tempo do quantum, então a UCP executa o próximo processo,
 mas se ele tiver tempo de execução maior do que o tempo do quantum, então o
 processo é interrompido e o próximo é executado para o mesmo tempo de quantum.
 Se um processo é interrompido, então uma troca de contexto acontece e o processo
 é colocado de volta no fim da fila. É de natureza preventiva.

 Este algoritmo depende principalmente do tempo do quantum. Com um tempo muito grande
 faz com o RR o mesmo que o FIFO, enquanto um tempo muito pequeno do quantum levará
 à sobrecarga com a troca de contexto que irá se repetir vez após vez em intervalos
 muito pequenos. A maior vantagem deste algoritmo é que todos os processos são
 executados um após o outro, o que não leva à fome de processos ou espera por processo
 por muito tempo para serem executados.

 TO_DO: /time/quantum
 ================================================================================
 */

#include<stdio.h>

int main(int argc, char *argv[]){
    int count, n, time, remain, flag=0, time_quantum;
    int wait_time=0, turnaround_time=0, at[10], bt[10], rt[10];

    printf("Insira o numero total de processos: ");
    scanf("%d",&n);

    remain=n;
    printf("\nInsira o tempo de chegada e o tempo de execucao\n");
    for(count=0;count<n;count++){
        printf("\nP[%d]\n",count+1);
        printf("Tempo-chegada : ");
        scanf("%d",&at[count]);
        printf("Tempo-execucao : ");
        scanf("%d",&bt[count]);
        rt[count]=bt[count];
    }

    printf("Insira o valor tempo do Quantum: ");
    scanf("%d",&time_quantum);

    printf("\nProcesso\tTempo-turnaround\tTempo-espera\n");
    for(time=0,count=0;remain!=0;){
        if(rt[count]<=time_quantum && rt[count]>0){
            time+=rt[count];
            rt[count]=0;
            flag=1;
        }
        else if(rt[count]>0){
            rt[count]-=time_quantum;
            time+=time_quantum;
        }

        if(rt[count]==0 && flag==1){
            remain--;
            printf("P[%d]\t\t%d\t\t\t%d\n",count+1,time-at[count],time-at[count]-bt[count]);
            wait_time+=time-at[count]-bt[count];
            turnaround_time+=time-at[count];
            flag=0;
        }
        if(count==n-1)
            count=0;
        else if(at[count+1]<=time)
            count++;
        else
            count=0;
    }

    printf("\nMedia do tempo de espera = %.5e\n",wait_time*1.0/n);
    printf("Media do tempo de turnaround = %.5e",turnaround_time*1.0/n);
    return 0;
}
