/*
 * main.c
 *
 *  Created on: 12 de apr. de 2019
 *      Author: anderson
 *      Description: Utilização de threads no padrão POSIX - PTHREAD para mostrar como é
 *      a criação, manipulação e teste
*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#define NUMBER_OF_THREADS 10

void *print_ola_mundo(void *tid){
    printf("Ola Mundo. Saudacoes do thread %d0", tid);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
    pthread_t threads[NUMBER_OF_THREADS];
    int status, i;
    for(i=0; i<NUMBER_OF_THREADS; i++){
        printf("Principal aqui criando thread %d0", i);
        status = pthread_create(&threads[i], NULL, print_ola_mundo, (void *)i);
        if (status != 0) {
            printf("Oops. pthread_create retornou codigo de erro %d0", status);
            exit(-1);
        }
    }
    exit(NULL); //pode ser return 0
}
