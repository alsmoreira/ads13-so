/* =====================================================================
 * main.c (padrão C99)
 *
 *  Created on: 11 de nov. de 2019
 *      Author: anderson
 *
 *  Description: Exemplo simples. O programa principal cria dois fluxos
 *  de execução usando a função pthread_create(). Você pode pensar em
 *  um fluxo de execução como uma função funcionando dentro do mesmo
 *  espaço de memória que outras funções, com mais de um deles ativo
 *  de cada vez. Neste exemplo, cada fluxo começa a funcionar em uma
 *  rotina chamada worker(), na qual ele simplesmente incrementa um contador
 *  em um loop para repetir determinado número de vezes.
 *  Uso: Para compilar utilizar algum sistema POSIX, neste caso utilizou
 *  o CYGWIN no Windows, digitar em um prompt $make e para executar
 *  ./concorrencia.exe 10000
 */


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "../include/comun.h"

volatile int contador = 0;
int loops;

void *worker(void *arg) {
    int i;
    for (i = 0; i < loops; i++) {
        contador++;
    }
    return NULL;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        fprintf(stderr, "uso: concorrencia <loops>\n");
        exit(1);
    }
    loops = atoi(argv[1]);
    pthread_t p1, p2;
    printf("Valor inicial : %d\n", contador);
    pthread_create(&p1, NULL, worker, NULL);
    pthread_create(&p2, NULL, worker, NULL);
    pthread_join(p1, NULL);
    pthread_join(p2, NULL);
    printf("Valor final   : %d\n", contador);
    return 0;
}
