/*
 * main.c
 *
 *  Created on: 15 de apr. de 2019
 *      Author: anderson
 *      Description: A memória compartilhada é uma das formas de comunicação
 *      entre processos (IPC) que permite que dois ou mais processos troquem dados
 *      e se comuniquem rapidamente no espaço do usuário. A memória compartilhada
 *      implica que vários processos compartilham a mesma região na memória, e
 *      eles podem modificar/acessar este segmento conforme necessário.
 *      O código implementa um uso básico de funções shmget e shmat para um processo
 *      que cria um novo segmento compartilhado e, em seguida, grava um texto nele.
 *      A função shmget leva três argumentos, o primeiro deles é a chave do segmento
 *      de memória. O valor-chave pode ser a macro IPC_PRIVATE se o novo segmento
 *      precisar ser criado ou o valor-chave do segmento existente quando o identificador
 *      de memória deve ser obtido com chamada de sistema. O segundo argumento do
 *      shmget especifica o tamanho do segmento e o terceiro argumento são as flags de
 *      permissão que podem incluir vários valores.
 *      Uma vez criado o segmento de memória, o identificador do segmento é obtido e
 *      pode ser passado para a função shmat para anexar a memória dada. O usuário pode
 *      passar o endereço específico onde anexar o segmento de memória dado como um
 *      segundo argumento para shmat. Ainda assim, geralmente, é preferível deixar o
 *      kernel escolher o endereço e especificar NULL para denotar isso.
 *      Teste foi realizado utilizando WSL versão 1.
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/shm.h>

enum {SEGMENT_SIZE = 0x6400}; //se for em binário 0110 0100 0000 0000

const char *data = "Ola!";

int main(int argc, char *argv[]) {

    int status; //não usado ainda por o código ser apenas de acesso à memória
    int segment_id;

    segment_id = shmget (IPC_PRIVATE, SEGMENT_SIZE, IPC_CREAT | IPC_EXCL | S_IRUSR | S_IWUSR);
    char *sh_mem = (char *) shmat(segment_id, NULL, 0);

    printf("ID do segmento %d\n", segment_id);
    printf("Anexado a %p\n", sh_mem);
    memmove(sh_mem, data, strlen(data)+1);
    printf("%s\n", sh_mem);

    shmdt(sh_mem); //desconecta o segmento de memória dado
    shmctl(segment_id, IPC_RMID, 0); //é usado modificar e desalocar o segmento com múltiplas opções.
    exit(EXIT_SUCCESS);
}
