/*
 ============================================================================
 Name        : main.c
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 17 jun 2018
 Copyright   :
 Description : Shortest Time-to-Completion First
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    int a[10], b[10], x[10];
    int waiting[10], turnaround[10], completion[10];
    int i, smallest, count=0, time, n;
    double avg=0,tt=0,end;

    printf("Entre com o numero de processos (max 10): ");
    scanf("%d",&n);

    printf("\nInsira o tempo de chegada e de execucao\n");
    for(i=0; i<n; i++) {
        printf("\nP[%d]\n", i+1);
        printf("Tempo-chegada : ");
        scanf("%d", &a[i]);
        printf("Tempo-execucao : ");
        scanf("%d", &b[i]);
        x[i] = b[i];
    }

    /*
	Coloque seu código aqui
	*/
	
    printf("\nProcesso\t tempo-execucao\t\t tempo-chegada\t\t tempo-espera\t\t turnaround\t\t tempo-final\n");
    for(i=0; i<n; i++){
    	printf("p[%d]\t\t %d\t\t\t %d\t\t\t %d\t\t\t %d\t\t\t %d\n",i+1,x[i],a[i],waiting[i],turnaround[i],completion[i]);
        avg = avg + waiting[i];
        tt = tt + turnaround[i];
    }
    printf("\n\nMedia tempo de espera = %.5e\n", avg);
    printf("Media tempo de turnaround = %.5e\n", tt);
    return 0;
}
