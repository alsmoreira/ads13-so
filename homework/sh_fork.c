/*
 * main.c
 *
 *  Created on: 18 de nov. de 2020
 *      Author: anderson
 *      Description: Criação de um shell de exemplo para demonstrar como é
 *      o init do sistema e sua interação com o fork()
 *      Use: Para compilar em um prompt, make sh_fork
 */

#include  <stdio.h>
#include  <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_INPUT_SIZE 1024
#define MAX_TOKEN_SIZE 64
#define MAX_NUM_TOKENS 64

/* Divide a sequência por espaço e retorna a matriz de tokens */

char **tokenize(char *line) {
    char **tokens = (char **)malloc(MAX_NUM_TOKENS * sizeof(char *));
    char *token = (char *)malloc(MAX_TOKEN_SIZE * sizeof(char));
    int i, tokenIndex = 0, tokenNo = 0;

    for(i =0; i < strlen(line); i++){
        char readChar = line[i];
        if (readChar == ' ' || readChar == '\n' || readChar == '\t'){
            token[tokenIndex] = '\0';
            if (tokenIndex != 0){
                tokens[tokenNo] = (char*)malloc(MAX_TOKEN_SIZE*sizeof(char));
                strcpy(tokens[tokenNo++], token);
                tokenIndex = 0;
            }
        } else {
            token[tokenIndex++] = readChar;
        }
    }

    free(token);
    tokens[tokenNo] = NULL ;
    return tokens;
}


int main(int argc, char* argv[]) {
    char  line[MAX_INPUT_SIZE];
    char  **tokens;
    int i;

    while(1) {
        /* INICIO: BUSCANDO ENTRADA */
        bzero(line, sizeof(line));
        printf("$ ");
        scanf("%[^\n]", line);
        getchar();

        printf("Comando inserido: %s (saida de depuracao)\n", line);
        /* FIM: BUSCANDO ENTRADA */

        line[strlen(line)] = '\n'; //finaliza com uma nova linha
        tokens = tokenize(line);

        //fazer o que você quiser com os comandos, aqui apenas imprimi

        for(i=0;tokens[i]!=NULL;i++){
            printf("token encontrado %s (saida de depuracao)\n", tokens[i]);
        }

        //Libera a memória alocada
        for(i=0;tokens[i]!=NULL;i++){
            free(tokens[i]);
        }
        free(tokens);

    }
    return 0;
}
