/*
 ============================================================================
 Name        : main.c (padrão C99)
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 15 de aug. de 2019
 Copyright   :
 Description : Este programa chama uma função Spin(). Esta verifica repetidamente
 o tempo e retorna sempre que se passa um segundo. Então, esta imprime na saída
 padrão, a string que o usuário passou como parâmetro em linha de comando e repete
 indefinidamente. Para compilar utilizar algum sistema POSIX, neste caso utilizou
 o CYGWIN no Windows, digitar em um prompt "make" e para executar "./cpu.exe A"
 Para informação dp GPROF: gprof cpu.exe gmon.out > analise.txt
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include "../include/comun.h"

int main(int argc, char *argv[])
{
    if (argc != 2) {
        fprintf(stderr, "uso: cpu <string>\n");
        exit(1);
    }
    char *str = argv[1];

    while (1) {
        printf("%s\n", str);
        Spin(1);
    }
    return 0;
}
