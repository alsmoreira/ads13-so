/*
 ============================================================================
 Name        : common.h
 Author      : Anderson Moreira
 E-mail		 : anderson.moreira@recife.ifpe.edu.br
 Version     : 25 de out. de 2018
 Copyright   : 
 Description : 
 ============================================================================
 */
#ifndef COMUN_H_
#define COMUN_H_

#include <sys/time.h>
#include <sys/stat.h>
#include <assert.h>
 
// A definição abaixo apenas se a função NULL não estiver definida no sistema
#ifndef NULL
#define NULL   ((void *) 0)
#endif

double GetTime() {
    struct timeval t;
    int rc = gettimeofday(&t, NULL);
    assert(rc == 0);
    return (double) t.tv_sec + (double) t.tv_usec/1e6;
}

void Spin(int howlong) {
    double t = GetTime();
    while ((GetTime() - t) < (double) howlong)
	; // nao faz nada no loop
}

#endif /* COMUN_H_ */
