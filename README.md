# Sistemas Operacionais (Introdução)

Repositório destinado aos códigos da disciplina de Sistemas Operacionais (ADS.13)

## Ementa

A ementa do curso está disponível no site [ALSM](https://gpads.recife.ifpe.edu.br/alsm/csin/index.php/ementa-so/)

## Conteúdo

O conteúdo é dividido em duas partes: A primeira explica os conceitos básicos de SO. O livro base para a disciplina é o de [Luiz Maia](http://www.training.com.br/aso/), no entanto os exemplos práticos em sua maioria vem do livro [Operating Systems: Three Easy Pieces (OSTEP)](http://pages.cs.wisc.edu/~remzi/OSTEP/). A segunda parte é a utilização do sistema operacional [SoTADS](https://gitlab.com/alsmoreira/SoTADS) disponível no gitlab. Este tem como fundamento o SO [xv6](https://github.com/mit-pdos/xv6-public), porém modificado para trabalhar com processadores de 64 bits. 

## Códigos

Lista em ordem de uso em sala de aula:

* [cpu](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/cpu) - Virtualização da UCP pelo SO.
* [mem](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/mem) - Virtualização da Memória pelo SO.
* [concorrencia](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/concorrencia) - Demonstração de conceitos iniciais sobre concorência em SO.
* [persistencia](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/persistencia) - Como ocorre a persistência de dados no SO de forma bem simples e resumida.
* [fork_1](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/fork_1) - Exemplo simples de utilização da chamada de sistemas fork().
* [fork_2](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/fork_2) - Exemplo simples de utilização da chamada de sistemas fork() com espera.
* [fork_3](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/fork_3) - Exemplo simples de utilização da chamada de sistemas fork() com execução de comando no filho.
* [fork_4](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/fork_4) - Exemplo simples de utilização da chamada de sistemas fork() com saída padrão para um arquivo.
* [fifo](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/fifo) - Algoritmo de escalonamento do tipo FIFO (First In First Out).
* [sjf](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/sjf) - Algoritmo de escalonamento do tipo SJF (Shortest Job First).
* [stcf](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/stcf) - Algoritmo de escalonamento do tipo STCF (Shortest Time-to-Completion First).
* [rr](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/rr) - Algoritmo de escalonamento do tipo RR (Round Robin).
* [psa](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/psa) - Algoritmo de escalonamento do tipo PSA (Priority Scheduling Algorithm).
* [thread_java](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/thread_java) - Exemplo simples de thread usando java.
* [thread_simples](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/thread_simples) - Exemplo simples de thread (POSIX - PTHREAD).
* [mem_comp1](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/mem_comp1) - Exemplo inicial de como utilizar memória compartilhada.
* [mem_comp2](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/mem_comp2) - Exemplo de memória compartilhada porém com uso de chamada de sistema fork e modificando a saída.
* [pipe](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/pipe) - Exemplo de comunicação entre processos utilizando o pipe e o descritor de arquivo. O exemplo é usado com o fork.

## Exercícios

* [homework](https://gitlab.com/alsmoreira/ads13-so/-/tree/main/homework) - Exercícios de sala de aula.
