
// Programa que cria um processo zumbi

#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int main(){
    // declaração de variáveis
    pid_t pid;          // definindo um identificador de processo - inteiro longo

    printf("Comecando o programa zumbi ... \n");
    printf( "Eu sou o PAI %d e vou criar um FILHO\n", getpid() );
    pid = fork();   // dividindo o processo em dois

    switch(pid){
        case -1:       // erro na abertura do processo filho
            exit(1);
        case 0:        // ações do Filho
            printf( "\t Eu sou o processo %d, FILHO DE %d \n", getpid(), getppid());
            printf( "\t Vou entrar num LOOP infinito\n");
            while(1);  // loop infinito
            break;
        default:      // ações do Pai
            sleep(3);
            printf(" Eu sou o processo PAI numero %d\n", getpid());
            printf(" Fim do Processo PAI\n");
            break;
    }
    system("ps ");
    printf("Terminando o programa zumbi ... \n");
    exit (0);

}
