/*
 * main.c
 *
 *  Created on: 22 de nov. de 2020
 *      Author: ander
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[])
{
    printf("ola mundo (pid:%d)\n", (int) getpid());
    int rc = fork();
    if (rc < 0) {
        fprintf(stderr, "fork falhou\n");
        exit(1);
    } else if (rc == 0) {
        printf("oi, eu sou um filho (pid:%d)\n", (int) getpid());
	    sleep(1);
    } else {
        int wc = wait(NULL);
        printf("oi, eu sou pai de %d (wc:%d) (pid:%d)\n", rc, wc, (int) getpid());
    }
    return 0;
}
