/*
 * main.c
 *
 *  Created on: 12 de nov. de 2020
 *      Author: ander
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
    printf("ola mundo (pid:%d)\n", (int) getpid());
    int rc = fork();
    if (rc < 0) {
        // fork falhou; sai
        fprintf(stderr, "fork falhou\n");
        exit(1);
    } else if (rc == 0) {
        // filho (novo processo)
        printf("oi, eu sou um filho (pid:%d)\n", (int) getpid());
    } else {
        // pai vai por esse caminho (processo original)
        printf("oi, eu sou pai de %d (pid:%d)\n",
	       rc, (int) getpid());
    }
    return 0;
}
