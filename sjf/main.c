/*
 ================================================================================================
 Name           : main.c
 Author         : Anderson Moreira
 E-mail         : anderson.moreira@recife.ifpe.edu.br
 Version        : 18 de mar de 2018
 Copyright      :
 Description    : Shortest Job First (SJF), também conhecido como Shortest Job Next (SJN)
 ou Shortest Process Next (SPN), é uma política de escalonamento que seleciona o processo em
 espera com o menor tempo de execução para executar em prioridade elevada. SJF é um algoritmo
 não-preemptivo. Um algoritmo variante do SJF é o shortest remaining time.

 O SJF é vantajoso por causa de sua simplicidade e por minimizar a quantidade média de tempo
 que cada processo tem que esperar até que sua execução seja concluída. No entanto, ele tem
 o potencial de starvation (espera indefinida) para processos que exigirão muito tempo para
 serem concluídos se processos curtos forem continuamente adicionados. A maior proporção de
 resposta em seguida é semelhante, mas fornece uma solução para este problema usando uma técnica
 chamada envelhecimento (aging).

 Outra desvantagem do uso do SJF é que o tempo total de execução de um trabalho deve ser conhecido
 antes da execução. Embora não seja possível prever perfeitamente o tempo de execução, vários
 métodos podem ser usados para estimar o tempo de execução de um trabalho, como uma média ponderada
 de tempos de execução anteriores.

 O SJF pode ser efetivamente usado com processos interativos que geralmente seguem um padrão de
 alternação entre esperar por um comando e executá-lo. Se o tempo de execução de um processo for
 considerada um "trabalho" separado, o comportamento passado pode indicar qual processo será
 executado em seguida, com base em uma estimativa de seu tempo de execução.
 É usado em ambientes especializados onde existem estimativas precisas do tempo de execução.
 ================================================================================================
 */

#include <stdio.h>
#include <time.h>

void SJF(int n) {
    int bt[20], p[20], wt[20], tat[20];
    int i, j, total = 0, pos, temp, loop = 1;
    float avg_wt, avg_tat;

    printf("\nDigite o tempo de execucao (u.t.) - apenas positivo:\n");

    while (loop == 1){
        for(i=0; i<n; i++){
            printf("p%d:", i+1);
            scanf("%d", &bt[i]);
            if(bt[i]<0){
                printf("\nValor incorreto\n");
                loop = 0;
            }
            else{
                p[i] = i+1; //contém o número do processo
            }
        }
        loop = 0;
    }

    //Classificação em ordem crescente de tempo usando o selection sort
    for(i=0; i<n; i++){
        pos=i;
        for(j=i+1; j<n; j++){
            if(bt[j] < bt[pos])
                pos=j;
        }

        temp=bt[i];
        bt[i]=bt[pos];
        bt[pos]=temp;

        temp=p[i];
        p[i]=p[pos];
        p[pos]=temp;
    }

    wt[0] = 0; //tempo de espera para o primeiro processo se tornar zero

    //calcula o tempo de espera
    for(i=1; i<n; i++){
        wt[i]=0;
        for(j=0; j<i; j++)
            wt[i]+=bt[j];

        total+=wt[i];
    }

    avg_wt = (float)total/n; //tempo de espera médio
    total=0;

    printf("\nProcesso\t  Tempo de Execucao\t   Tempo de Espera\tTurnaround");
    for(i=0;i<n;i++){
        tat[i]=bt[i]+wt[i]; //calcula o tempo de turnaround
        total+=tat[i];
        printf("\np%d\t\t  %d\t\t\t    %d\t\t\t%d", p[i], bt[i], wt[i], tat[i]);
    }

    avg_tat = (float)total / n; //tempo médio de turnaround dos processos

    printf("\n\nMedia do tempo de espera = %f (u.t.)", avg_wt);
    printf("\nMedia do tempo de turnaround = %f (u.t.)\n", avg_tat);
}

int main(int argc, char *argv[]){
    int num;
    double tm;
    //clock_t starttime, endtime;
    clock_t t1, t2;

    //calculo em nanosegundos
    struct timespec tps, tpe;
    if ((clock_gettime(CLOCK_REALTIME, &tps) != 0) || (clock_gettime(CLOCK_REALTIME, &tpe) != 0)) {
        perror("clock_gettime");
        return -1;
    }
    //printf("%lu s, %lu ns\n", tpe.tv_sec-tps.tv_sec, tpe.tv_nsec-tps.tv_nsec); - não utiliza mais
    //fim

    printf("Digite o numero de processos: maximo de 20");
    scanf("%d", &num);

    //'t1' e 't2' são usados apenas em milisegundos. Resultado é armazenado em 'tm'
    t1 = clock(); //starttime = tps.tv_nsec;
    SJF(num);
    t2 = clock(); //endtime = tpe.tv_nsec;

    tm = (1000.0 * ((double)t2 - (double)t1) / (double)CLOCKS_PER_SEC);
    //para o printf '%.3e mili seg or'

    //printf ("Tempo de Execucao =  %lu nano seg\n", endtime-starttime);
    printf ("Tempo de Execucao do Programa =  %.5e mili seg\n\n", tm);

    return 0;
}
