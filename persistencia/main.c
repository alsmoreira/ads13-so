/*
 * main.c
 *
 *  Created on: 11 de nov. de 2020
 *      Author: ander
 *
 *  Description: Teste de persistencia em sistemas operacionais
 *  Use: Testado em máquina Windows com Cygwin (POSIX - padrão C99)
 *  Uso: Para compilar em um prompt digite $make e execute ./persistencia.exe
 */

#include <assert.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {
    int fd = open("/home/ander/file", O_WRONLY|O_CREAT|O_TRUNC,S_IRWXU);
    assert(fd > -1);
    int rc = write(fd, "anderson\n", 12);
    assert(rc == 12);
    close(fd);
    return 0;
}
